# README #

This is the source code for the ArduIMU that allows it to collect and transmit data from a BNO055.


### How do I get set up? ###
This project can be opened with the Arduino IDE, but it requires an ArduIMU v4+

In order to log data, a second ArduIMU (or Zigduino) must be attached to your PC with a serial port and it must be running the RFtoSerial project.