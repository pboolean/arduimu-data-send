#include "BNO055.h"
#include <Wire.h>
#include "IMUPacket.h"
#include <ZigduinoRadio.h>

BNO055 imu_bno055;
BNO055 imu_bno055_extern;

#define ARDUIMU_ID 12

void setup()
{

	Serial.begin(115200);

	// just let me know that it's alive...
	//Serial.println("Paul is cool"); // so cool

	RF.begin(11);
	RF.setParam(RP_TXPWR(3));
	RF.setParam(RP_DATARATE(MOD_OQPSK_1000));
	
	BNO055::BNO055StatusData status = imu_bno055.begin(BNO055::NDOF, BNO055::FASTEST_MODE, BNO055_ADDRESS_A);
	//BNO055::BNO055StatusData statusExt = imu_bno055_extern.begin(BNO055::IMU, BNO055::FASTEST_MODE, BNO055_ADDRESS_B);

	char statusDataMsg[150];
	int msg_sz;
	
	msg_sz= sprintf(&statusDataMsg[0], "INTERNAL:\nSystem Status Code: %02X\nSelf-test result: %02X\nSystem Error result: %02X\nChip ID: %02X\nSoftwareRev: %04X\nBootloader: %02X\n",
		status.SystemStatusCode,
		status.SelfTestStatus,
		status.SystemError,
		status.ChipID,
		status.SoftwareRevision,
		status.BootLoaderVersion);

	Serial.write(statusDataMsg);

	/*msg_sz = sprintf(&statusDataMsg[0], "EXTERNAL:\nSystem Status Code: %02X\nSelf-test result: %02X\nSystem Error result: %02X\nChip ID: %02X\nSoftwareRev: %04X\nBootloader: %02X\n",
		statusExt.SystemStatusCode,
		statusExt.SelfTestStatus,
		statusExt.SystemError,
		statusExt.ChipID,
		statusExt.SoftwareRevision,
		statusExt.BootLoaderVersion);

	Serial.write(statusDataMsg);*/

	pinMode(13, OUTPUT);
}

void checkCalibration()
{
	BNO055::BNO055CalibrationStatus calibrationExtern = imu_bno055_extern.readCalibrationState();

	char statusDataMsg[150];
	uint8_t msg_sz;

	msg_sz = sprintf(&statusDataMsg[0], "Time: %lu\nSys Cal: %d\nGyro Cal: %d\nAcc Cal: %d\nMag Cal: %d\nstat: %02X\n",
		millis(),
		calibrationExtern.sys_calib_status,
		calibrationExtern.gyro_calib_status,
		calibrationExtern.acc_calib_status,
		calibrationExtern.mag_calib_status,
		calibrationExtern.calib_stat_contents);

	//RF.beginTransmission();
	//RF.write((uint8_t*)statusDataMsg, msg_sz);
	//RF.endTransmission();

	Serial.write((uint8_t*)statusDataMsg, (msg_sz));	

	if (calibrationExtern.acc_calib_status == 3 && calibrationExtern.gyro_calib_status == 3)
	{
		BNO055::BNO055CalibrationData calibData = imu_bno055_extern.readCalibrationData();
		char statusDataMsg2[200];
		msg_sz = sprintf(&statusDataMsg2[0], "Calibration Params:\nAccel: X=%d Y=%d Z=%d\nGyro: X=%d Y=%d Z=%d\nMag: X=%d Y=%d Z=%d\nAccel Radius: %d  Magnetometer Radius: %d\n",
			calibData.acc_offset_x,
			calibData.acc_offset_y,
			calibData.acc_offset_z,
			
			calibData.gyr_offset_x,
			calibData.gyr_offset_y,
			calibData.gyr_offset_z,

			calibData.mag_offset_x,
			calibData.mag_offset_y,
			calibData.mag_offset_z,
			
			calibData.acc_radius,
			calibData.mag_radius);

		Serial.write((uint8_t*)statusDataMsg2, (msg_sz));
		//RF.beginTransmission();
		//RF.write((uint8_t*)statusDataMsg2, msg_sz);
		//RF.endTransmission();
	}
}

long lastCalibCheck = 0;

void loop()
{
	long timestamp = millis();

	//BNO055::BNO055QuaData quaDataExtern = imu_bno055_extern.readQua();
	//BNO055::BNO055LinAccData accelDataExtern = imu_bno055_extern.readLinAcc();
	
	BNO055::BNO055QuaData quaDataLocal = imu_bno055.readQua();
	BNO055::BNO055LinAccData accelDataLocal = imu_bno055.readLinAcc();

	//ArduIMUPacket packetIMUExtern = createIMUBinaryPacket(ARDUIMU_ID, timestamp, quaDataExtern);
	//ArduIMUPacket packetAccelExtern = createAccelBinaryPacket(ARDUIMU_ID, timestamp, accelDataExtern);

	ArduIMUPacket packetIMULocal = createIMUTextPacket(ARDUIMU_ID, timestamp, quaDataLocal);
	ArduIMUPacket packetAccelLocal = createAccelTextPacket(ARDUIMU_ID, timestamp, accelDataLocal);

	//digitalWrite(13, HIGH);
		
	//RF transmission
	//RF.beginTransmission();
	//RF.write(packetIMUExtern.rawData, packetIMULocal.size);
	//RF.write(packetAccelExtern.rawData, packetAccelExtern.size);
	//RF.endTransmission();

	RF.beginTransmission();
	RF.write(packetIMULocal.rawData, packetIMULocal.size);
	RF.write(packetAccelLocal.rawData, packetAccelLocal.size);
	RF.endTransmission();

	//digitalWrite(13, LOW);

	
	//for (int index = 0; index < packetIMULocal.size; index++)
	//{
	//	int dataByte = packetIMULocal.rawData[index];
	//	Serial.write(dataByte);
	//}
	
	delay(12);

	//for (int index = 0; index < packetAccelLocal.size; index++)
	//{
	//	int dataByte = packetAccelLocal.rawData[index];
	//	Serial.write(dataByte);
	//}

	////Serial.println("-------packet--------");
	//int written = Serial.write(&packetIMUExtern.rawData[0], packetIMUExtern.size);
	//if (written != packetIMUExtern.size)
	//{
	//	Serial.println("ERROR: DID NOT WRITE PACKET GUD");
	//}



	////Serial.println("-------packet--------");
	//Serial.write(packetAccel.rawData, packetAccel.size);

	//Serial.print("IMU size: ");
	//Serial.println(packetIMU.size);

	//Serial.print("accel size: ");
	//Serial.println(packetAccel.size);


	

	////Serial.println("________________");
	
	//if ((timestamp - lastCalibCheck) > 1000) // check every second
	//{
	//	lastCalibCheck = timestamp;

	//	//checkCalibration();
	//	BNO055::BNO055CalibrationStatus calibrationExtern = imu_bno055_extern.readCalibrationState();
	//	ArduIMUPacket packetCalibExtern = createCalibStatBinaryPacket(0, timestamp, calibrationExtern);

	//	//BNO055::BNO055CalibrationStatus calibrationLocal = imu_bno055.readCalibrationState();
	//	//ArduIMUPacket packetCalibLocal = createCalibStatBinaryPacket(1, timestamp, calibrationLocal);

	//	RF.beginTransmission();
	//	RF.write(packetCalibExtern.rawData, packetCalibExtern.size);
	//	//RF.write(packetCalibLocal.rawData, packetCalibLocal.size);

	//	RF.endTransmission();
	//}

}
