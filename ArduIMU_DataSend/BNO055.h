/***************************************************
	This is a library for the BNO055
	It is based upon code written by Andrew Harmon
	that is available here:
	https://github.com/andrewsharmon/BNO055
	****************************************************/
#ifndef __BNO055_H__
#define __BNO055_H__

#if (ARDUINO >= 100)
#include "Arduino.h"
#else
#include "WProgram.h"
#endif
//#include "Wire.h"
#include <Wire.h> // if in sketch directory

#define BNO055_ADDRESS_A                (0x28)        // 0x28 com3 low 0x29 com3 high
#define BNO055_ADDRESS_B                (0x29)        // 0x28 com3 low 0x29 com3 high

#define BNO055_POLL_TIMEOUT           (100)         // Maximum number of read attempts
#define BNO055_ID                     (0xA0)        //pg58


class BNO055
{
public:

	/************************************
		Register IDs (From Datasheet)
		************************************/
	typedef enum
	{													// DEFAULT    TYPE
		//page0
		BNO055_REGISTER_CHIP_ID = 0x00,					// 0x00       r
		BNO055_REGISTER_ACC_ID = 0x01,					// 0xFB       r
		BNO055_REGISTER_MAG_ID = 0x02,					// 0x32       r
		BNO055_REGISTER_GYR_ID = 0x03,					// 0x0F       r
		BNO055_REGISTER_SW_REV_ID_LSB = 0x04,			//            r        
		BNO055_REGISTER_SW_REV_ID_MSB = 0x05,			//            r
		BNO055_REGISTER_BL_REV_ID = 0x06,				//            r
		BNO055_REGISTER_PAGE_ID = 0x07,					// 0x00       rw
		BNO055_REGISTER_ACC_DATA_X_LSB = 0x08,			// 0x00       r
		BNO055_REGISTER_ACC_DATA_X_MSB = 0x09,			// 0x00       r
		BNO055_REGISTER_ACC_DATA_Y_LSB = 0x0A,			// 0x00       r
		BNO055_REGISTER_ACC_DATA_Y_MSB = 0x0B,			// 0x00       r
		BNO055_REGISTER_ACC_DATA_Z_LSB = 0x0C,			// 0x00       r
		BNO055_REGISTER_ACC_DATA_Z_MSB = 0x0D,			// 0x00       r
		BNO055_REGISTER_MAG_DATA_X_LSB = 0x0E,			// 0x00       r
		BNO055_REGISTER_MAG_DATA_X_MSB = 0x0F,			// 0x00       r
		BNO055_REGISTER_MAG_DATA_Y_LSB = 0x10,			// 0x00       r
		BNO055_REGISTER_MAG_DATA_Y_MSB = 0x11,			// 0x00       r
		BNO055_REGISTER_MAG_DATA_Z_LSB = 0x12,			// 0x00       r
		BNO055_REGISTER_MAG_DATA_Z_MSB = 0x13,			// 0x00       r
		BNO055_REGISTER_GYR_DATA_X_LSB = 0x14,			// 0x00       r
		BNO055_REGISTER_GYR_DATA_X_MSB = 0x15,			// 0x00       r
		BNO055_REGISTER_GYR_DATA_Y_LSB = 0x16,			// 0x00       r
		BNO055_REGISTER_GYR_DATA_Y_MSB = 0x17,			// 0x00       r
		BNO055_REGISTER_GYR_DATA_Z_LSB = 0x18,			// 0x00       r
		BNO055_REGISTER_GYR_DATA_Z_MSB = 0x19,			// 0x00       r
		BNO055_REGISTER_EUL_DATA_X_LSB = 0x1A,			// 0x00       r
		BNO055_REGISTER_EUL_DATA_X_MSB = 0x1B,			// 0x00       r
		BNO055_REGISTER_EUL_DATA_Y_LSB = 0x1C,			// 0x00       r
		BNO055_REGISTER_EUL_DATA_Y_MSB = 0x1D,			// 0x00       r
		BNO055_REGISTER_EUL_DATA_Z_LSB = 0x1E,			// 0x00       r
		BNO055_REGISTER_EUL_DATA_Z_MSB = 0x1F,			// 0x00       r
		BNO055_REGISTER_QUA_DATA_W_LSB = 0x20,			// 0x00       r
		BNO055_REGISTER_QUA_DATA_W_MSB = 0x21,			// 0x00       r
		BNO055_REGISTER_QUA_DATA_X_LSB = 0x22,			// 0x00       r
		BNO055_REGISTER_QUA_DATA_X_MSB = 0x23,			// 0x00       r
		BNO055_REGISTER_QUA_DATA_Y_LSB = 0x24,			// 0x00       r
		BNO055_REGISTER_QUA_DATA_Y_MSB = 0x25,			// 0x00       r
		BNO055_REGISTER_QUA_DATA_Z_LSB = 0x26,			// 0x00       r
		BNO055_REGISTER_QUA_DATA_Z_MSB = 0x27,			// 0x00       r
		BNO055_REGISTER_LIA_DATA_X_LSB = 0x28,			// 0x00       r
		BNO055_REGISTER_LIA_DATA_X_MSB = 0x29,			// 0x00       r
		BNO055_REGISTER_LIA_DATA_Y_LSB = 0x2A,			// 0x00       r
		BNO055_REGISTER_LIA_DATA_Y_MSB = 0x2B,			// 0x00       r
		BNO055_REGISTER_LIA_DATA_Z_LSB = 0x2C,			// 0x00       r
		BNO055_REGISTER_LIA_DATA_Z_MSB = 0x2D,			// 0x00       r
		BNO055_REGISTER_GRV_DATA_X_LSB = 0x2E,			// 0x00       r
		BNO055_REGISTER_GRV_DATA_X_MSB = 0x2F,			// 0x00       r
		BNO055_REGISTER_GRV_DATA_Y_LSB = 0x30,			// 0x00       r
		BNO055_REGISTER_GRV_DATA_Y_MSB = 0x31,			// 0x00       r
		BNO055_REGISTER_GRV_DATA_Z_LSB = 0x32,			// 0x00       r
		BNO055_REGISTER_GRV_DATA_Z_MSB = 0x33,			// 0x00       r
		BNO055_REGISTER_TEMP = 0x34,					// 0x00       r
		BNO055_REGISTER_CALIB_STAT = 0x35,				// 0x00       r
		BNO055_REGISTER_ST_RESULT = 0x36,				// xxxx1111   r
		BNO055_REGISTER_INT_STA = 0x37,					// 000x00xx   r  pg74
		BNO055_REGISTER_SYS_CLK_STATUS = 0x38,			// 00000000   r  pg74
		BNO055_REGISTER_SYS_STATUS = 0x39,				// 00000000   r  pg74
		BNO055_REGISTER_SYS_ERR = 0x3A,					// 00000000   r  pg75
		BNO055_REGISTER_UNIT_SEL = 0x3B,				// 0xx0x000   rw pg76
		BNO055_REGISTER_OPR_MODE = 0x3D,				// x???????   rw pg77
		BNO055_REGISTER_PWR_MODE = 0x3E,				// xxxxxx??   rw pg78
		BNO055_REGISTER_SYS_TRIGGER = 0x3F,				// 000xxxx0   w  pg78
		BNO055_REGISTER_TEMP_SOURCE = 0x40,				// xxxxxx??   rw pg78
		BNO055_REGISTER_AXIS_MAP_CONFIG = 0x41,			// xx??????   rw pg79
		BNO055_REGISTER_AXIS_MAP_SIGN = 0x42,			// xxxxx???   rw pg79
		BNO055_REGISTER_SIC_MATRIX = 0x43,				// xxxxxx??   ?? pg80
		BNO055_REGISTER_ACC_OFFSET_X_LSB = 0x55,		// 0x00       rw
		BNO055_REGISTER_ACC_OFFSET_X_MSB = 0x56,		// 0x00       rw
		BNO055_REGISTER_ACC_OFFSET_Y_LSB = 0x57,		// 0x00       rw
		BNO055_REGISTER_ACC_OFFSET_Y_MSB = 0x58,		// 0x00       rw
		BNO055_REGISTER_ACC_OFFSET_Z_LSB = 0x59,		// 0x00       rw
		BNO055_REGISTER_ACC_OFFSET_Z_MSB = 0x5A,		// 0x00       rw
		BNO055_REGISTER_MAG_OFFSET_X_LSB = 0x5B,		// 0x00       rw
		BNO055_REGISTER_MAG_OFFSET_X_MSB = 0x5C,		// 0x00       rw
		BNO055_REGISTER_MAG_OFFSET_Y_LSB = 0x5D,		// 0x00       rw
		BNO055_REGISTER_MAG_OFFSET_Y_MSB = 0x5E,		// 0x00       rw
		BNO055_REGISTER_MAG_OFFSET_Z_LSB = 0x5F,		// 0x00       rw
		BNO055_REGISTER_MAG_OFFSET_Z_MSB = 0x60,		// 0x00       rw
		BNO055_REGISTER_GYR_OFFSET_X_LSB = 0x61,		// 0x00       rw
		BNO055_REGISTER_GYR_OFFSET_X_MSB = 0x62,		// 0x00       rw
		BNO055_REGISTER_GYR_OFFSET_Y_LSB = 0x63,		// 0x00       rw
		BNO055_REGISTER_GYR_OFFSET_Y_MSB = 0x64,		// 0x00       rw
		BNO055_REGISTER_GYR_OFFSET_Z_LSB = 0x65,		// 0x00       rw
		BNO055_REGISTER_GYR_OFFSET_Z_MSB = 0x66,		// 0x00       rw
		BNO055_REGISTER_ACC_RADIUS_LSB = 0x67,			// 0x00       rw
		BNO055_REGISTER_ACC_RADIUS_MSB = 0x68,			// 0x00       rw
		BNO055_REGISTER_MAG_RADIUS_LSB = 0x69,			// 0x00       rw
		BNO055_REGISTER_MAG_RADIUS_MSB = 0x6A,			// 0x00       rw


		//page 1

//		BNO055_REGISTER_PAGE_ID =			0x07,		// ??         rw	// re-defined for completeness, see page 0
		BNO055_REGISTER_ACC_CONFIG =		0x08,		// 00001101   rw pg87
		BNO055_REGISTER_MAG_CONFIG =		0x09,		// 00001011   rw pg87
		BNO055_REGISTER_GYR_CONFIG =		0x0A,		// 00111000   rw pg88
		BNO055_REGISTER_GYR_CONFIG_1 =		0x0B,		// 00000000   rw pg88
		BNO055_REGISTER_ACC_SLEEP_CONFIG =	0x0C,		// ????????   rw pg89
		BNO055_REGISTER_GYR_SLEEP_CONFIG =	0x0D,		// ????????   rw pg90
		BNO055_REGISTER_INT_MSK =			0x0F,		// 000x00xx   rw pg91
		BNO055_REGISTER_INT_EN =			0x10,		// 000x00xx   rw pg92
		BNO055_REGISTER_ACC_AM_THRES =		0x11,		// 00010100   rw pg92
		BNO055_REGISTER_ACC_INT_SETTINGS =	0x12,		// 00000011   rw pg93
		BNO055_REGISTER_ACC_HG_DURATION =	0x13,		// 00001111   rw pg93
		BNO055_REGISTER_ACC_HG_THRES =		0x14,		// 11000000   rw pg93
		BNO055_REGISTER_ACC_NM_THRES =		0x15,		// 00001010   rw pg93
		BNO055_REGISTER_ACC_NM_SET =		0x16,		// x0001011   rw pg94
		BNO055_REGISTER_GYR_INT_SETTING =	0x17,		// 00000000   rw pg95
		BNO055_REGISTER_GYR_HR_X_SET =		0x18,		// 00000001   rw pg95
		BNO055_REGISTER_GYR_DUR_X =			0x19,		// 00011001   rw pg96
		BNO055_REGISTER_ACC_HR_Y_SET =		0x1A,		// 00000001   rw pg96
		BNO055_REGISTER_GYR_DUR_Y =			0x1B,		// 00011001   rw pg96
		BNO055_REGISTER_ACC_HR_Z_SET =		0x1C,		// 00000001   rw pg97
		BNO055_REGISTER_GYR_DUR_Z =			0x1D,		// 00011001   rw pg97
		BNO055_REGISTER_GYR_AM_THRES =		0x1E,		// 00000100   rw pg97
		BNO055_REGISTER_GYR_AM_SET =		0x1F		// 00001010   rw pg98
	} BNO055Registers_t;


	/************************************
		Register values (From Datasheet) 
	************************************/
	//TODO: Add register ENUMs as needed
	typedef enum
	{												//HW SENS POWER     SENS SIG        FUSION
													//  A   M   G       A   M   G       E   Q   L   G
		CONFIGMODE =	0x00,	//	0b00000000		//  y   y   y       n   n   n       n   n   n   n
		ACCONLY =		0x01,	//	0b00000001		//  y   n   n       y   n   n       n   n   n   n
		MAGONLY =		0x02,	//	0b00000010		//  n   y   n       n   y   n       n   n   n   n
		GYROONLY =		0x03,	//	0b00000011		//  n   n   y       n   n   y       n   n   n   n
		ACCMAG =		0x04,	//	0b00000100		//  y   y   n       y   y   n       n   n   n   n
		ACCGYRO =		0x05,	//	0b00000101		//  y   n   y       y   n   y       n   n   n   n
		MAGGYRO =		0x06,	//	0b00000110		//  n   y   y       n   y   y       n   n   n   n
		AMG =			0x07,	//	0b00000111		//  y   y   y       y   y   y       n   n   n   n
		IMU =			0x08,	//	0b00001000		//  y   n   y       y   n   y       y   y   y   y
		COMPASS =		0x09,	//	0b00001001		//  y   y   n       y   y   n       y   y   y   y
		M4G =			0x0a,	//	0b00001010		//  y   y   n       y   y   y       y   y   y   y
		NDOF_FMC_OFF =	0x0b,	//	0b00001011		//  y   y   y       y   y   y       y   y   y   y
		NDOF =			0x0c	//	0b00001100		//  y   y   y       y   y   y       y   y   y   y
	} BNO055Mode_t;

	typedef enum
	{
		NORMAL_POWER_MODE =		0x00,	// 0b00000000
		LOW_POWER_MODE =		0x01,	// 0b00000001
		SUSPEND_POWER_MODE =	0x02	// 0b00000010
	} BNO055PowerModes_t;

	// I'm not sure that this one came from the datasheet
	typedef enum
	{
		FASTEST_MODE =	(uint8_t)(1 << 5),						// 0b00100000
		GAME_MODE =		(uint8_t)(1 << 6),						// 0b01000000
		UI_MODE =		(uint8_t)(FASTEST_MODE | GAME_MODE),	// 0b01100000
		NORMAL_MODE =	(uint8_t)(1 << 7)						// 0b10000000,
	} BNO055DataRateMode_t;

	typedef enum
	{
		CLK_SEL =	(uint8_t)(1 << 7),
		RST_INT =	(uint8_t)(1 << 6),
		RST_SYS =	(uint8_t)(1 << 5),
		SELF_TEST =	(uint8_t)(1 << 0)
	} BNO055SystemTrigger_t;

	typedef enum
	{
		ACC_Unit_METRES_PER_SEC2 = 0,
		ACC_Unit_MILLI_G = (uint8_t)(1 << 0),

		GYR_Unit_DEG_PER_SEC = 0,
		GYR_Unit_RAD_PER_SEC = (uint8_t)(1 << 1),

		EUL_Unit_DEG = 0,
		EUL_Unit_RAD = (uint8_t)(1 << 2),

		TEMP_Unit_CELSIUS = 0,
		TEMP_Unit_FAHRENHEIT = (uint8_t)(1 << 4),

		ORI_Windows = 0,					// turning clockwise decreases values
		ORI_Android = (uint8_t)(1 << 7),	// turning clockwise increases values

	} BNO055UnitSel_t;


	/************************************
		Custom Structures
	************************************/

#define NORMALIZER_EULER 16
#define Z_CORR 5760 // 360 * 16 = 5760, offset
#define X_CORR 5760 // 360 * 16 = 5760, offset
#define Y_CORR 1440 // 90 * 16 = 1440
	typedef struct BNO055EulerData_s
	{
		float x; // degrees
		float y; // degrees
		float z; // degrees

		int16_t x_int;	// 1/16 degrees
		int16_t y_int;	// 1/16 degrees
		int16_t z_int;	// 1/16 degrees

	} BNO055EulerData;

#define NORMALIZER_ACCEL 100.0f
	typedef struct BNO055LinAccData_s
	{
		float x; // m/s ^2
		float y; // m/s ^2
		float z; // m/s ^2

		int16_t x_int;	// 1/100 m/s ^2
		int16_t y_int;	// 1/100 m/s ^2
		int16_t z_int;	// 1/100 m/s ^2
		bool isGravCompensated;
	} BNO055LinAccData;


#define NORMALIZER_QUATERNION 16384.0f
	typedef struct BNO055QuaData_s
	{
		int16_t w_int;	// 1/16384 Quaterion units
		int16_t x_int;	// 1/16384 Quaterion units
		int16_t y_int;	// 1/16384 Quaterion units
		int16_t z_int;	// 1/16384 Quaterion units

		float w;
		float x;
		float y;
		float z;
	} BNO055QuaData;

	typedef struct BNO055StatusData_s
	{
		uint8_t SystemStatusCode;
		uint8_t SelfTestStatus;
		uint8_t SystemError;
		uint8_t ChipID;
		uint16_t SoftwareRevision;
		uint8_t BootLoaderVersion;

	} BNO055StatusData;

	typedef struct BNO055CalibrationStatus_s
	{
		// Current system calibration status that depends upon the status of all sensors, read only.
		uint8_t sys_calib_status;

		// current calibration status of gyroscope. 3 indicates fully calibrated, 0 indicates uncalibrated
		uint8_t gyro_calib_status;

		// current calibration status of accelerometer. 3 indicates fully calibrated, 0 indicates uncalibrated
		uint8_t acc_calib_status;

		// current calibration status of magnetometer. 3 indicates fully calibrated, 0 indicates uncalibrated
		uint8_t mag_calib_status;

		// the contents of the CALIB_STAT register
		uint8_t calib_stat_contents;

	} BNO055CalibrationStatus;

	typedef struct BNO055CalibrationData_s
	{
		int16_t acc_offset_x, acc_offset_y, acc_offset_z;

		int16_t gyr_offset_x, gyr_offset_y, gyr_offset_z;

		int16_t mag_offset_x, mag_offset_y, mag_offset_z;

		int16_t acc_radius, mag_radius;
	} BNO055CalibrationData;


	/***********************************
		Function Signatures
	***********************************/
	BNO055(void);
	BNO055::BNO055StatusData begin(BNO055Mode_t md = NDOF, BNO055DataRateMode_t spd = FASTEST_MODE, uint8_t addr = BNO055_ADDRESS_A);
	BNO055EulerData readEulerPhone(void);
	BNO055EulerData readEulerAbsolute(void);
	BNO055StatusData getInfo(void);
	BNO055LinAccData readAcc(void);
	BNO055LinAccData readLinAcc(void);
	uint8_t readError(void);
	BNO055QuaData readQua(void);
	BNO055LinAccData calcAbsLinAcc(void);
	BNO055CalibrationStatus readCalibrationState(void);
	BNO055CalibrationData readCalibrationData(void);

	/**********************************
		Instance Variables
	**********************************/
	BNO055EulerData EulerDataPhone;	// Last read will be available here
	BNO055EulerData EulerDataAbs;	// Last read will be available here
	BNO055LinAccData LinAccData;	// Last read will be available here
	BNO055LinAccData AbsLinAccData;	//TODO: Get rid of
	BNO055QuaData QuaData;			// Last read will be available here
	BNO055CalibrationData CalibData;// Last read will be available here

private:
	void write8(BNO055Registers_t reg, uint8_t value);
	BNO055EulerData readEuler(bool absolute);
	uint8_t read8(BNO055Registers_t reg);
	uint8_t bno055_addr;
	BNO055DataRateMode_t dataSpeed;
	BNO055Mode_t mode;
	uint8_t message;
	BNO055StatusData StatusData;	// Last read will be available here

	BNO055CalibrationStatus CalibrationStatus;
};

#endif
