#ifndef __IMUPACKET_H__
#define __IMUPACKET_H__

#define DEFAULT_DELIMITER ','
#define MAX_MSG_SIZE 100
#define HEADER_SIZE 8
#define PACKET_VERSION 1 // increment each time the format changes (max 255)

#include <stdint.h>
#include "BNO055.h"

typedef enum PacketVariant_t
{
	// Orientation in the form of a Quaternion
	IMU_DATA = 'Q',

	// Acceleration Data in the form of a 3-vector (m/s^2)
	ACCEL_DATA = 'A',

	// Calibration state
	CALIBRATION_STATE = 'L',

	// Control frame
	CONTROL = 'C',

	// Displacement data, computed using Zero-velocity UPdaTe (ZUPT)
	ZUPT_DISPLACEMENT = 'Z',

	// unknown
	UNKNOWN = 'X'

}PacketVariant;

typedef struct ArduIMUPacket_s
{
	uint8_t rawData[MAX_MSG_SIZE];
	int size;
}ArduIMUPacket;

//---------------------------------
// Current version of the protocol
//---------------------------------
uint8_t version();

//--------------------------------------------------
// Convert a 4-byte signed float into a 
// 32-bit representation suitable for transmission
// (Big-Endian)
//--------------------------------------------------
int32_t floatToBytes(float f);

//--------------------------------------------------
// create a packet that can be transmitted.
//--------------------------------------------------
ArduIMUPacket createBinaryPacket(uint8_t sensorID, PacketVariant variant, unsigned long time, int16_t* sensorValues);
ArduIMUPacket createIMUBinaryPacket(uint8_t sensorID, unsigned long time, BNO055::BNO055QuaData imuData);
ArduIMUPacket createAccelBinaryPacket(uint8_t sensorID, unsigned long time, BNO055::BNO055LinAccData accelData);
ArduIMUPacket createCalibStatBinaryPacket(uint8_t sensorID, unsigned long time, BNO055::BNO055CalibrationStatus calStatus);


ArduIMUPacket createTextPacket(uint8_t sensorID, PacketVariant variant, unsigned long time, int16_t* sensorValues);
ArduIMUPacket createIMUTextPacket(uint8_t sensorID, unsigned long time, BNO055::BNO055QuaData imuData);
ArduIMUPacket createAccelTextPacket(uint8_t sensorID, unsigned long time, BNO055::BNO055LinAccData accelData);
ArduIMUPacket createCalibStatTextPacket(uint8_t sensorID, unsigned long time, BNO055::BNO055CalibrationStatus calStatus);
#endif