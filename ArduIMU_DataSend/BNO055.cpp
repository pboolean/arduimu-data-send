/**************************************************
  This is a library for the BNO055
  **************************************************/


//#include <BNO055.h>
#include "BNO055.h" // if in sketch directory

//--------------------------------
//	Default Constructor
//--------------------------------
BNO055::BNO055(void)
{
	// use i2c
}

//--------------------------------------------------------------------------------
// Initialize the BNO-055
// Return '0' if everything initialized OK, error code indicates what went wrong.
//--------------------------------------------------------------------------------
BNO055::BNO055StatusData BNO055::begin(BNO055Mode_t md, BNO055DataRateMode_t spd, uint8_t addr)
{
	/**************************
		instance variables
	***************************/

	//TWBR = 400000L; // enable I2C-Fast

	if (TWCR == 0) // do this check so that Wire only gets initialized once
	{
		Serial.println("Beginning wire library...");
		Wire.begin();
	}
	mode = md;
	bno055_addr = addr;
	dataSpeed = spd;
	message = 0x00;


	/**************************
		Setup
	***************************/
	delay(700); // datasheet reports startup time is 650 ms

	write8(BNO055_REGISTER_OPR_MODE, CONFIGMODE); //Go to config mode if not there

	while (read8(BNO055_REGISTER_CHIP_ID) != BNO055_ID) //wait for it to be ready
	{
		delay(10);
	}
	delay(50);

	Serial.print("Resetting BNO055 at address ");
	Serial.println(bno055_addr, HEX);
	// now reset it to get into a known state


	/* This section of code apparently this makes the BNO055 on address 29 take forever to boot. */
	//uint8_t reset = read8(BNO055_REGISTER_SYS_TRIGGER);
	//write8(BNO055_REGISTER_SYS_TRIGGER, (reset | RST_SYS));

	//// now wait for it to start up again
	//delay(2000); // datasheet reports startup time is 650 ms

	////write8(BNO055_REGISTER_OPR_MODE, CONFIGMODE); //Go to config mode if not there
	//

	//Serial.println("wait for ID");
	//while (read8(BNO055_REGISTER_CHIP_ID) != BNO055_ID) //wait for it to be ready
	//{
	//	delay(50);
	//}
	//delay(50);
	/* end delaying code */

	Serial.println("Configuring");
	// begin configuring

	// Default to normal power
	write8(BNO055_REGISTER_PWR_MODE, NORMAL_POWER_MODE);
	delay(50);

	// set units to metric (we are not in the USA)
	write8(BNO055_REGISTER_UNIT_SEL, ORI_Windows | ACC_Unit_METRES_PER_SEC2 | TEMP_Unit_CELSIUS | GYR_Unit_DEG_PER_SEC | EUL_Unit_DEG);
	delay(50);

	// set axis to match ArduIMU board orientation
	write8(BNO055_REGISTER_AXIS_MAP_CONFIG, 0x24);
	delay(50);
	write8(BNO055_REGISTER_AXIS_MAP_SIGN, 0x00); // all positive
	delay(50);

	// set operating mode as defined by user
	write8(BNO055_REGISTER_OPR_MODE, mode);
	delay(50);

	return getInfo();
}

/***************************************************************************
 PUBLIC FUNCTIONS
 ***************************************************************************/
//------------------------------------------------------------------------------------------
// Get the fused orientation in Euler angles (within the absolute reference frame)
// the angle is about a particular axis, e.g. the X component represents the rotation about X in the YZ plane.
// for each axis angles range from 0-360, with rotations in the counter-clockwise direction
// increasing the angle.
// exception is the Y axis which ranges from -90 to + 90. the quadrant can be determined by inspecting the X angle,
// which flips to 180 as Y crosses 90.
//                   
//                  Z(+)
//                   |   
//          Y=+60 \  |  / Y = +60
//          X=180  \ | /  X = 0
//                  \|/
// X(+) -------------Y-------------- X(-)
//                   |
//                   |
//                   |
//                  Z(-)
//------------------------------------------------------------------------------------------
BNO055::BNO055EulerData BNO055::readEulerAbsolute()
{
	return readEuler(true);
}

BNO055::BNO055EulerData BNO055::readEuler(bool absolute)
{
	uint8_t heading_MSB, heading_LSB, roll_LSB, roll_MSB, pitch_LSB, pitch_MSB;


	Wire.beginTransmission(bno055_addr);
	// Make sure to set address auto-increment bit
	Wire.write(BNO055_REGISTER_EUL_DATA_X_LSB);
	Wire.endTransmission();
	Wire.requestFrom(bno055_addr, (uint8_t)6);

	// Wait around until enough data is available
	while (Wire.available() < 6);

	heading_LSB = Wire.read();
	heading_MSB = Wire.read();
	roll_LSB = Wire.read();
	roll_MSB = Wire.read();
	pitch_LSB = Wire.read();
	pitch_MSB = Wire.read();

	BNO055EulerData euler_data;

	// Shift values to create properly formed integer (low byte first)
	euler_data.z_int = (heading_LSB | (heading_MSB << 8));
	euler_data.x_int = (pitch_LSB | (pitch_MSB << 8));
	euler_data.y_int = (roll_LSB | (roll_MSB << 8));

	if (absolute)
	{

		// fix data range
		// BNO055 reports Z in the range 0, +360, where CW is positive
		euler_data.z_int = Z_CORR - euler_data.z_int;

		// BNO055 reports X in the range -180, +180, where CW is negative
		euler_data.x_int = (euler_data.x_int < 0) ?
			(euler_data.x_int + X_CORR) :
			euler_data.x_int; // leave as is

		// BNO055 reports Y in the range -90, +90, where CW is positive
		euler_data.y_int = -euler_data.y_int; // now CW is negative.

		// set output
		euler_data.x = (float)euler_data.x_int / 16.0f;
		euler_data.y = (float)euler_data.y_int / 16.0f;
		euler_data.z = (float)euler_data.z_int / 16.0f;

		EulerDataAbs.x = (float)euler_data.x_int / 16.0f;
		EulerDataAbs.y = (float)euler_data.y_int / 16.0f;
		EulerDataAbs.z = (float)euler_data.z_int / 16.0f;

		EulerDataAbs.x_int = euler_data.x_int;
		EulerDataAbs.y_int = euler_data.y_int;
		EulerDataAbs.z_int = euler_data.z_int;
	}
	else
	{
		// set output
		euler_data.x = (float)euler_data.x_int / 16.0f;
		euler_data.y = (float)euler_data.y_int / 16.0f;
		euler_data.z = (float)euler_data.z_int / 16.0f;

		EulerDataPhone.x = (float)euler_data.x_int / 16.0f;
		EulerDataPhone.y = (float)euler_data.y_int / 16.0f;
		EulerDataPhone.z = (float)euler_data.z_int / 16.0f;

		EulerDataPhone.x_int = euler_data.x_int;
		EulerDataPhone.y_int = euler_data.y_int;
		EulerDataPhone.z_int = euler_data.z_int;
	}


	return euler_data;
}

//------------------------------------------------------------------------------------------
// Get the fused orientation in Euler angles (within the android phone reference frame)
// the angle is about a particular axis, e.g. the X component represents the rotation about X in the YZ plane.
// X: range = +180, -180
// Y: range = +90, -90
// Z: range = 0, 360
//------------------------------------------------------------------------------------------
BNO055::BNO055EulerData BNO055::readEulerPhone()
{
	return readEuler(false);
}

//--------------------------------------------
// Get the fused absolute linear acceleration, in m/s^2
//--------------------------------------------
BNO055::BNO055LinAccData BNO055::readLinAcc()
{
	uint8_t xhi, xlo, ylo, yhi, zlo, zhi;


	Wire.beginTransmission(bno055_addr);
	// Make sure to set address auto-increment bit
	Wire.write(BNO055_REGISTER_LIA_DATA_X_LSB);
	Wire.endTransmission();
	Wire.requestFrom(bno055_addr, (byte)6);

	// Wait around until enough data is available
	while (Wire.available() < 6);

	xlo = Wire.read();
	xhi = Wire.read();
	ylo = Wire.read();
	yhi = Wire.read();
	zlo = Wire.read();
	zhi = Wire.read();

	// Shift values to create properly formed integer (low byte first)
	LinAccData.x_int = (xlo | (xhi << 8));
	LinAccData.y_int = (ylo | (yhi << 8));
	LinAccData.z_int = (zlo | (zhi << 8));

	LinAccData.x = (float)LinAccData.x_int / NORMALIZER_ACCEL;
	LinAccData.y = (float)LinAccData.y_int / NORMALIZER_ACCEL;
	LinAccData.z = (float)LinAccData.z_int / NORMALIZER_ACCEL;

	LinAccData.isGravCompensated = true;

	return LinAccData;
}

//-------------------------------------------
// Get un-fused linear acceleration: Plain vanilla
//-------------------------------------------
BNO055::BNO055LinAccData BNO055::readAcc()
{
	uint8_t xhi, xlo, ylo, yhi, zlo, zhi;


	Wire.beginTransmission(bno055_addr);
	// Make sure to set address auto-increment bit
	Wire.write(BNO055_REGISTER_ACC_DATA_X_LSB);
	Wire.endTransmission();
	Wire.requestFrom(bno055_addr, (byte)6);

	// Wait around until enough data is available
	while (Wire.available() < 6);

	xlo = Wire.read();
	xhi = Wire.read();
	ylo = Wire.read();
	yhi = Wire.read();
	zlo = Wire.read();
	zhi = Wire.read();

	// Shift values to create properly formed integer (low byte first)
	LinAccData.x_int = (xlo | (xhi << 8));
	LinAccData.y_int = (ylo | (yhi << 8));
	LinAccData.z_int = (zlo | (zhi << 8));

	LinAccData.x = (float)LinAccData.x_int / NORMALIZER_ACCEL;
	LinAccData.y = (float)LinAccData.y_int / NORMALIZER_ACCEL;
	LinAccData.z = (float)LinAccData.z_int / NORMALIZER_ACCEL;

	LinAccData.isGravCompensated = false;

	return LinAccData;
}

//---------------------------------------------------------
// Get a quaternion representing the device's orientation.
//---------------------------------------------------------
BNO055::BNO055QuaData BNO055::readQua()
{
	uint8_t whi, wlo, xhi, xlo, ylo, yhi, zlo, zhi;

	Wire.beginTransmission(bno055_addr);
	// Make sure to set address auto-increment bit
	Wire.write(BNO055_REGISTER_QUA_DATA_W_LSB);
	Wire.endTransmission();
	Wire.requestFrom(bno055_addr, (byte)8);

	// Wait around until enough data is available
	while (Wire.available() < 8);

	wlo = Wire.read();
	whi = Wire.read();
	xlo = Wire.read();
	xhi = Wire.read();
	ylo = Wire.read();
	yhi = Wire.read();
	zlo = Wire.read();
	zhi = Wire.read();


	QuaData.w_int = (wlo | (whi << 8));
	QuaData.x_int = (xlo | (xhi << 8));
	QuaData.y_int = (ylo | (yhi << 8));
	QuaData.z_int = (zlo | (zhi << 8));

	// Shift values to create properly formed integer (low byte first) then scale (1qua=2^14lsb)
	QuaData.w = QuaData.w_int / NORMALIZER_QUATERNION;
	QuaData.x = QuaData.x_int / NORMALIZER_QUATERNION;
	QuaData.y = QuaData.y_int / NORMALIZER_QUATERNION;
	QuaData.z = QuaData.z_int / NORMALIZER_QUATERNION;

	return QuaData;
}

BNO055::BNO055CalibrationData BNO055::readCalibrationData()
{

	// switch to config mode
	write8(BNO055_REGISTER_OPR_MODE, CONFIGMODE);

	uint8_t hi, low;

	Wire.beginTransmission(bno055_addr);
	// Make sure to set address auto-increment bit
	Wire.write(BNO055_REGISTER_ACC_OFFSET_X_LSB);
	Wire.endTransmission();
	Wire.requestFrom(bno055_addr, (uint8_t)22);

	// Wait around until enough data is available
	while (Wire.available() < 22);

	// ACCELEROMETER OFFSETS
	low = Wire.read();
	hi = Wire.read();

	CalibData.acc_offset_x = (low | (hi << 8));

	low = Wire.read();
	hi = Wire.read();

	CalibData.acc_offset_y = (low | (hi << 8));

	low = Wire.read();
	hi = Wire.read();

	CalibData.acc_offset_z = (low | (hi << 8));


	// MAGNETOMETER OFFSETS
	low = Wire.read();
	hi = Wire.read();

	CalibData.mag_offset_x = (low | (hi << 8));

	low = Wire.read();
	hi = Wire.read();

	CalibData.mag_offset_y = (low | (hi << 8));

	low = Wire.read();
	hi = Wire.read();

	CalibData.mag_offset_z = (low | (hi << 8));


	// GYROSCOPE OFFSETS
	low = Wire.read();
	hi = Wire.read();

	CalibData.gyr_offset_x = (low | (hi << 8));

	low = Wire.read();
	hi = Wire.read();

	CalibData.gyr_offset_y = (low | (hi << 8));

	low = Wire.read();
	hi = Wire.read();

	CalibData.gyr_offset_z = (low | (hi << 8));

	// RADIUS PARAMETERS
	low = Wire.read();
	hi = Wire.read();

	CalibData.acc_radius = (low | (hi << 8));

	low = Wire.read();
	hi = Wire.read();

	CalibData.mag_radius = (low | (hi << 8));

	// back to fusion mode
	write8(BNO055_REGISTER_OPR_MODE, mode);

	return CalibData;
}

BNO055::BNO055LinAccData BNO055::calcAbsLinAcc()  //uses last loaded QuaData and LinAccData.  ie, make sure reads for those have been called...
{
	float a1, b1, c1, d1, a2, b2, c2, d2, ra, rb, rc, rd, den; //RAM waste, but helps make it look cleaner

	//stuff my quaternions and acceleration vectors into some variables to operate on them
	//specifically calc q^-1.  Note, sensor seems to send the inverse.
	den = pow(QuaData.w, 2) + pow(QuaData.x, 2) + pow(QuaData.y, 2) + pow(QuaData.z, 2); // the passed quaternion 'should' be good, but just in case check

	if (den<1.01 && den>.99)  //close enough lets save some processing 
	{
		a1 = QuaData.w;
		b1 = QuaData.x;   //not negative because of how sensor sends the values
		c1 = QuaData.y;   //not negative because of how sensor sends the values
		d1 = QuaData.z;   //not negative because of how sensor sends the values

	}
	else{ //oh well, time to divide some floats
		a1 = QuaData.w / den;
		b1 = QuaData.x / den;   //not negative because of how sensor sends the values
		c1 = QuaData.y / den;   //not negative because of how sensor sends the values
		d1 = QuaData.z / den;   //not negative because of how sensor sends the values
	}
	//load accel vector V
	a2 = 0;
	b2 = LinAccData.x;
	c2 = LinAccData.y;
	d2 = LinAccData.z;

	// time to Hamilton it up! (q^-1 * V)
	ra = a1*a2 - b1*b2 - c1*c2 - d1*d2;
	rb = a1*b2 + b1*a2 + c1*d2 - d1*c2;
	rc = a1*c2 - b1*d2 + c1*a2 + d1*b2;
	rd = a1*d2 + b1*c2 - c1*b2 + d1*a2;

	//swap some vars
	//first invert q
	a2 = a1;
	b2 = -b1;
	c2 = -c1;
	d2 = -d1;
	//now move the result
	a1 = ra;
	b1 = rb;
	c1 = rc;
	d1 = rd;


	//Hamilton it up again! (result*q)
	ra = a1*a2 - b1*b2 - c1*c2 - d1*d2;
	rb = a1*b2 + b1*a2 + c1*d2 - d1*c2;
	rc = a1*c2 - b1*d2 + c1*a2 + d1*b2;
	rd = a1*d2 + b1*c2 - c1*b2 + d1*a2;

	//finally (probably many clock cycles later) set the final values
	AbsLinAccData.x = rb;
	AbsLinAccData.y = rc;
	AbsLinAccData.z = rd;
	AbsLinAccData.isGravCompensated = true;

	return AbsLinAccData;
}

typedef struct vector3_s
{
	int16_t x, y, z;
} vector3;

vector3 crossProduct(vector3 a, vector3 b)
{
	vector3 result;

	result.x = (a.y * b.z) - (a.z * b.y);
	result.y = (a.x * b.z) - (a.z * b.x);
	result.z = (a.x * b.y) - (a.y * b.x);

	return result;
}

//-----------------------------------------------
//	Request system information from BNO055 chip
//-----------------------------------------------
BNO055::BNO055StatusData BNO055::getInfo()
{
	StatusData.SystemStatusCode = read8(BNO055_REGISTER_SYS_STATUS);
	StatusData.SelfTestStatus = read8(BNO055_REGISTER_ST_RESULT);
	StatusData.SystemError = read8(BNO055_REGISTER_SYS_ERR);
	StatusData.BootLoaderVersion = read8(BNO055_REGISTER_BL_REV_ID);
	StatusData.SoftwareRevision = (read8(BNO055_REGISTER_SW_REV_ID_MSB) << 8) | read8(BNO055_REGISTER_SW_REV_ID_LSB);
	StatusData.ChipID = read8(BNO055_REGISTER_CHIP_ID);

	return StatusData;
}

//-------------------
// Check for errors (faster than getting all the system information
//-------------------
uint8_t BNO055::readError()
{
	return read8(BNO055_REGISTER_SYS_ERR);
}

//--------------------------------------------
// Check calibration status for each sensor
//--------------------------------------------
BNO055::BNO055CalibrationStatus BNO055::readCalibrationState()
{
	CalibrationStatus.calib_stat_contents = read8(BNO055_REGISTER_CALIB_STAT);

	CalibrationStatus.sys_calib_status = (CalibrationStatus.calib_stat_contents >> 6) & 0x03; // bits 7:6
	CalibrationStatus.gyro_calib_status = (CalibrationStatus.calib_stat_contents >> 4) & 0x03; // bits 5:4
	CalibrationStatus.acc_calib_status = (CalibrationStatus.calib_stat_contents >> 2) & 0x03; // bits 3:2
	CalibrationStatus.mag_calib_status = (CalibrationStatus.calib_stat_contents >> 0) & 0x03; // bits 1:0

	return CalibrationStatus;
}

/***************************************************************************
 PRIVATE FUNCTIONS
 ***************************************************************************/

//---------------------------------------------------
//	Write 8 bits to a register in the BNO055
//---------------------------------------------------
void BNO055::write8(BNO055Registers_t reg, byte value)
{
	// use i2c
	Wire.beginTransmission(bno055_addr);
	Wire.write((byte)reg);
	Wire.write(value);
	Wire.endTransmission();
}

//---------------------------------------------------
//	Read 8 bits from a register in the BNO055.
//	Return byte that was read
//---------------------------------------------------
byte BNO055::read8(BNO055Registers_t reg)
{
	byte value;


	// use i2c
	Wire.beginTransmission(bno055_addr);
	Wire.write((byte)reg);
	Wire.endTransmission();
	Wire.requestFrom(bno055_addr, (byte)1);
	value = Wire.read();
	Wire.endTransmission(false);

	return value;
}