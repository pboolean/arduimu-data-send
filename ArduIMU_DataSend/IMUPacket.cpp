#include "IMUPacket.h"
#include "Arduino.h"


int32_t floatToBytes(float f)
{
	int32_t convertedNumber = 0;

	// get access to the float as a byte-array:
	uint8_t *data = (uint8_t *)&f;

	if (sizeof(f) <= sizeof(int32_t))
	{
		convertedNumber |= (((int32_t)data[3] << 0 * 8)) & (0x000000ff);
		convertedNumber |= (((int32_t)data[2] << 1 * 8)) & (0x0000ff00);
		convertedNumber |= (((int32_t)data[1] << 2 * 8)) & (0x00ff0000);
		convertedNumber |= (((int32_t)data[0] << 3 * 8)) & (0xff000000);

		// THIS DOES NOT WORK EASILY
		//for (uint8_t byteIndex = 0; byteIndex < sizeof(f); byteIndex++)
		//{
		//	convertedNumber |= ((data[byteIndex]) << byteIndex);
		//	Serial.print("Converted number: ");
		//	Serial.println(convertedNumber);
		//}
	}

	return convertedNumber;
}

uint8_t version()
{
	return 0;
}

//--------------------------------------------------
// create a string that can be transmitted.
// note that the string is newline-terminated.
// fields are comma-separated and follow the general structure as shown:
//
//	+-------------------------------------------------------------------------------------------------------+
//  | version | packet type | sensor ID |	time	| <packet data> (comma-separated byte-encoded floats)	|
//	+-------------------------------------------------------------------------------------------------------+
//
//--------------------------------------------------
ArduIMUPacket createTextPacket(uint8_t sensorID, PacketVariant variant, unsigned long time, float* sensorValues)
{
	char header[MAX_MSG_SIZE];
	int headerLen = sprintf(header, "%d,%c,%d,%lu", version(), variant, sensorID, time);

	//Serial.println("header:");
	//Serial.print("\t");
	//Serial.println(header);

	char payload[MAX_MSG_SIZE];
	int payloadLen = 0;

	switch (variant)
	{
	case ACCEL_DATA:
		payloadLen = sprintf(payload, "%08lx,%08lx,%08lx",
			floatToBytes(sensorValues[0]),
			floatToBytes(sensorValues[1]),
			floatToBytes(sensorValues[2])
			);
		break;

	case CONTROL:
		break;

	case CALIBRATION_STATE:
		payloadLen = sprintf(payload, "%02X,%d,%d,%d,%d",
			(uint8_t) sensorValues[0], // actually bytes
			(uint8_t) sensorValues[1],
			(uint8_t) sensorValues[2],
			(uint8_t) sensorValues[3],
			(uint8_t) sensorValues[4]
			);
		break;
	case IMU_DATA:
		payloadLen = sprintf(payload, "%08lx,%08lx,%08lx,%08lx",
			floatToBytes(sensorValues[0]),
			floatToBytes(sensorValues[1]),
			floatToBytes(sensorValues[2]),
			floatToBytes(sensorValues[3])
			);
		break;

	default:
		break;
	}

	//Serial.println("payload:");
	//Serial.print("\t");
	//Serial.println(payload);

	ArduIMUPacket packet;
	int packetLen = snprintf((char *)packet.rawData, MAX_MSG_SIZE, "%s,%s,\n", header, payload);
	packet.size = packetLen;

	//Serial.print("Size: ");
	//Serial.println(packet.size);

	return packet;
}

//--------------------------------------------------
// create a byte array that can be transmitted.
// fields are one byte long (except as noted) and follow the general structure as shown:
//
//	+---------------------------------------------------------------------------------------------+
//  | version | packet type (char) | sensor ID |	time (uint32)	| data length | <packet data> |
//	+---------------------------------------------------------------------------------------------+
//
//--------------------------------------------------
ArduIMUPacket createBinaryPacket(uint8_t sensorID, PacketVariant variant, unsigned long time, uint8_t* sensorValues)
{
	//Serial.println("header:");
	//Serial.print("\t");
	//Serial.println(header);

	uint8_t payload[MAX_MSG_SIZE];
	int payloadLen = 0;

	switch (variant)
	{
	case ACCEL_DATA: // 3 x int16 = 6 bytes
		payloadLen = 6;
		break;

	case CALIBRATION_STATE: // a single byte
		payloadLen = 1;
		break;

	case CONTROL:
		break;

	case IMU_DATA: // 4 x int 16 = 8 bytes
		payloadLen = 8;
		break;

	default:
		break;
	}

	uint8_t *time_addr = (uint8_t *)&time;

	uint8_t header[HEADER_SIZE];
	header[0] = PACKET_VERSION;
	header[1] = variant;
	header[2] = sensorID;
	header[3] = (time_addr)[0];
	header[4] = (time_addr)[1];
	header[5] = (time_addr)[2];
	header[6] = (time_addr)[3];
	header[7] = payloadLen;


	ArduIMUPacket packet;
	memcpy(&(packet.rawData[0]), &(header[0]), HEADER_SIZE);
	memcpy(&(packet.rawData[HEADER_SIZE]), &(sensorValues[0]), payloadLen);
	packet.size = payloadLen + HEADER_SIZE;

	return packet;
}

//--------------------------------------------------
// create a binary-representation of an IMU data packet that can be transmitted.
// Time can be whatever unit you want, provided both endpoints are in agreement.
//--------------------------------------------------
ArduIMUPacket createIMUBinaryPacket(uint8_t sensorID, unsigned long time, BNO055::BNO055QuaData imuData)
{
	int16_t imu_orientation_data_i16[] =
	{
		imuData.w_int,
		imuData.x_int,
		imuData.y_int,
		imuData.z_int
	};

	/*int16_t imu_orientation_data_i16[] =
	{
	1,
	-1,
	1,
	-1
	};*/
	return createBinaryPacket(sensorID, IMU_DATA, time, (uint8_t *)imu_orientation_data_i16);
}

//--------------------------------------------------
// create a binary-representation of a Calibration State data packet that can be transmitted.
// Time can be whatever unit you want, provided both endpoints are in agreement.
//--------------------------------------------------
ArduIMUPacket createCalibStatBinaryPacket(uint8_t sensorID, unsigned long time, BNO055::BNO055CalibrationStatus calibrationStatus)
{
	uint8_t calibrationStatusBytes[] =
	{
		calibrationStatus.calib_stat_contents
	};
	return createBinaryPacket(sensorID, CALIBRATION_STATE, time, calibrationStatusBytes);
}

//--------------------------------------------------
// create a binary-representation of an acceleration data packet that can be transmitted.
// Time can be whatever unit you want, provided both endpoints are in agreement.
//--------------------------------------------------
ArduIMUPacket createAccelBinaryPacket(uint8_t sensorID, unsigned long time, BNO055::BNO055LinAccData accelData)
{
	int16_t accel_data_i16[] =
	{
		accelData.x_int,
		accelData.y_int,
		accelData.z_int
	};

	return createBinaryPacket(sensorID, ACCEL_DATA, time, (uint8_t *)accel_data_i16);
}

//--------------------------------------------------
// create a text-representation of an IMU data packet that can be transmitted.
// Time can be whatever unit you want, provided both endpoints are in agreement.
//--------------------------------------------------
ArduIMUPacket createIMUTextPacket(uint8_t sensorID, unsigned long time, BNO055::BNO055QuaData imuData)
{
	float imu_orientation_data_f[] =
	{
		imuData.w,
		imuData.x,
		imuData.y,
		imuData.z
	};
	return createTextPacket(sensorID, IMU_DATA, time, imu_orientation_data_f);
}


//--------------------------------------------------
// create a text-representation of an acceleration data packet that can be transmitted.
// Time can be whatever unit you want, provided both endpoints are in agreement.
//--------------------------------------------------
ArduIMUPacket createAccelTextPacket(uint8_t sensorID, unsigned long time, BNO055::BNO055LinAccData accelData)
{
	float accel_data_f[] =
	{
		accelData.x,
		accelData.y,
		accelData.z
	};
	return createTextPacket(sensorID, ACCEL_DATA, time, accel_data_f);
}

//--------------------------------------------------
// create a binary-representation of a Calibration State data packet that can be transmitted.
// Time can be whatever unit you want, provided both endpoints are in agreement.
//--------------------------------------------------
ArduIMUPacket createCalibStatTextPacket(uint8_t sensorID, unsigned long time, BNO055::BNO055CalibrationStatus calibrationStatus)
{
	float calibrationStatusVals[] =
	{
		(float) calibrationStatus.calib_stat_contents, // cast as floats for compatibility with the function
		(float) calibrationStatus.sys_calib_status,
		(float) calibrationStatus.gyro_calib_status,
		(float) calibrationStatus.acc_calib_status,
		(float) calibrationStatus.mag_calib_status
	};
	return createTextPacket(sensorID, CALIBRATION_STATE, time, calibrationStatusVals);
}